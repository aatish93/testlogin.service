const { create, getUsers, getUserById, updateUser, deleteUser } = require('./user.service');
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const { sign } = require('jsonwebtoken');
module.exports = {
    createUser: (req,res)=>{
        const body=req.body;
        body.password=hashSync(body.password,genSaltSync(10));
        create(body,(err,results)=>{
            if(err){
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: "Database connection error"
                });
            }
            console.log('res : ',results);
            return res.status(200).json({
                success: 1,
                data: results
            });
        });
    },
    getUsers: (req,res) => {
        getUsers((err,results) => {
            if(err){
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getUserById: (req,res) => {
        const id = req.params.id;
        getUserById(id,(err,results) => {
            if(err){
                console.log(err);
                return;
            }
            if(!results){
                return res.json({
                    success: 0,
                    message: "User not found"
                });
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    updateUser: (req,res) => {
        const body=req.body;
        body.password = hashSync(body.password,genSaltSync(10));
        updateUser(body,(err,results) => {
            if(err){
                console.log(err);
                return;
            }
            if(!results){
                return res.json({
                    success: 0,
                    message: "User not found"
                });
            }
            return res.json({
                success: 1,
                message: "Updated successfully"
            });
        });
    },
    deleteUser: (req,res) => {
        const body=req.body;
        deleteUser(body,(err,results) => {
            if(err){
                console.log(err);
                return;
            }
            if(!results){
                return res.json({
                    success: 0,
                    message: "User not found"
                });
            }
            return res.json({
                success: 1,
                message: "Deleted successfully"
            });
        });
    },
    login: (req,res) => {
        const body = req.body;
        getUserById(body.id,(err,results) => {
            if(err){
                console.log(err);
                return;
            }
            console.log('results ',results);
            if(!results){
                return res.json({
                    success: 0,
                    message: "Invalid username or password"
                });
            }
            const result=compareSync(body.password,results.password);
            if(result){
                results.password=undefined;
                const jsonwebtoken=sign({ result: results }, "qwerty123", {
                    expiresIn: "1h"
                });
                return res.json({
                    success: 1,
                    message: "Login Successful",
                    token: jsonwebtoken
                });
            } else {
                return res.json({
                    success: 0,
                    message: "Invalid username or password"
                });
            }
        });
    }
}