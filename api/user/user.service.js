const pool=require("../../config/database");

module.exports = {
    create: (data, callback) => {
        pool.query(
            `insert into user(name,password,address,gender) values(?,?,?,?)`,
            [
                data.name, 
                data.password,
                data.address,
                data.gender
            ],
            (error, results, fields)=>{
                if(error) {
                    return callback(error);
                }
                return callback(null,results);
            }
        );
    },
    getUsers: callback => {
        pool.query(
            `select name,address,gender from user`,
            [],
            (error, results, fields)=>{
                if(error) {
                    return callback(error);
                }
                return callback(null,results);
            }
        );
    },
    getUserById: (id,callback) => {
        pool.query(
            `select name,password,address,gender from user where id = ?`,
            [id],
            (error, results, fields)=>{
                if(error) {
                    return callback(error);
                }
                return callback(null,results[0]);
            }
        );
    },
    updateUser: (data,callback) => {
        pool.query(
            `update user set name=?, password=?, address=?, gender=? where id=?`,
            [
                data.name,
                data.password,
                data.address,
                data.gender,
                data.id
            ],
            (error,results,fields) => {
                if(error) {
                    return callback(error);
                }
                return callback(null,results);
            }
        );
    },
    deleteUser: (data,callback) => {
        pool.query(
            `delete from user where id = ?`,
            [data.id],
            (error, results, fields)=>{
                if(error) {
                    return callback(error);
                }
                return callback(null,results[0]);
            }
        );
    }
}